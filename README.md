This is a docker image for the Minecraft Bedrock Edition Alpha server.
The server and it's documentation is located at:
https://www.minecraft.net/en-us/download/server/bedrock/

An example docker build script is provided in `build.sh`.
This uses the `VERSION` to specify the alpha server version.

The bedrock server is configured via a server.properties file with
properties in the syntax
`some-property-name=some-property-value`
The `config_gen.sh` script will automatically take environment variables
of the format
`SOME_PROPERTY_NAME=some-property-value`
and replace the server.properties configuration with the environment
specified value.

The docker container uses two volumes.  One for the config files and one
for the world data files.

See `start.sh` for an example of how the container can be started with
pre-created volumes and how the environment variable configuration works
