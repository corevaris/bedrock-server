FROM ubuntu:latest

EXPOSE 19132/udp
EXPOSE 19133/udp

ARG VERSION=1.10.0.7
ARG UID=10000
ARG GID=10000

RUN apt-get update && apt-get install -y \
    unzip \
    curl \
    && apt-get -y dist-upgrade && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /bedrock-server \
    && curl -SL https://minecraft.azureedge.net/bin-linux/bedrock-server-${VERSION}.zip -o /bedrock-server/bedrock-server.zip \
    && unzip /bedrock-server/bedrock-server.zip -d /bedrock-server \
    && rm /bedrock-server/bedrock-server.zip

RUN groupadd -g ${GID} minecraft \
    && useradd -u ${UID} -g ${GID} -d /bedrock-server -M minecraft \
    && chown -R minecraft:minecraft /bedrock-server

COPY entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh

COPY config_gen.sh /config_gen.sh
RUN chmod 755 /config_gen.sh

WORKDIR /bedrock-server

VOLUME /config
VOLUME /bedrock-server/worlds

ENV LD_LIBRARY_PATH=/bedrock-server
ENTRYPOINT ["/entrypoint.sh"]
CMD ["/bedrock-server/bedrock_server"]
