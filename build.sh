#!/bin/sh -eu
VERSION=$(cat VERSION)
DOCKER_CMD=docker
${DOCKER_CMD} pull ubuntu:latest
${DOCKER_CMD} build --build-arg="VERSION=${VERSION}" -t bedrock-server:"${VERSION}" .
${DOCKER_CMD} tag bedrock-server:"${VERSION}" bedrock-server:latest
