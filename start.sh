#!/bin/sh
DOCKER_CMD=docker
${DOCKER_CMD} create --name minecraft_server -p 19132-19133:19132-19133/udp -v minecraft_worlds:/bedrock-server/worlds -v minecraft_config:/config -e GAMEMODE=creative -e ALLOW_CHEATS=true -e ONLINE_MODE=false -e SERVER_NAME="$(hostname)" -e DEFAULT_PLAYER_PERMISSION_LEVEL=operator bedrock-server:latest
