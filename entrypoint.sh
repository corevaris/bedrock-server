#!/bin/sh

apt-get update && apt-get -y dist-upgrade

CONFIG_FILES="permissions.json whitelist.json server.properties"

mkdir -p /config/orig

for i in ${CONFIG_FILES}; do
  if [ ! -f "/config/${i}" ]; then
    cp -p "/bedrock-server/${i}" /config
  fi
  if [ -f "/bedrock-server/${i}" ] && [ ! -L "/bedrock-server/${i}" ]; then
    mv -f "/bedrock-server/${i}" /config/orig
  fi
  ln -sf "/config/${i}" "/bedrock-server/${i}"
done

/config_gen.sh /config/server.properties

chown -R minecraft:minecraft /config /bedrock-server/worlds

exec su minecraft -c "$@"
