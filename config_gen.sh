#!/bin/bash
function get_variables() {
  grep -oe '^.*=' server.properties | cut -f1 -d= | tr '[:lower:] -' '[:upper:] _'
}

function env2key() {
  echo "$1" | tr '[:upper:] _' '[:lower:] -'
}

config_file=${1}
for config in $(get_variables); do
  if [ -n "${!config}" ]; then
    key=$(env2key "${config}")
    new_config=$(env2key "$config")=${!config}
    sed -i "s/${key}=.*/${new_config}/" "${config_file}"
  fi
done
